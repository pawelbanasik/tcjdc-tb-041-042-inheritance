
public class App {

	public static void main(String[] args) {

		Animal animal1 = new Animal("Animal", 1, 1, 5, 5);
		Dog dog1 = new Dog("Yorkie", 8, 20, 2, 4, 1, 20, 0, 0, "long silky");

		dog1.eat();
		dog1.walk();
		dog1.run();
		
	}

}
